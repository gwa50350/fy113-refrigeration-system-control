### 除霜排程 ###

#           電磁閥   壓縮機   風扇     除霜電熱
#
# 0s         OFF      OFF     OFF      OFF
#
# 10s        OFF      OFF     OFF      ON
#
# DAG>-50    OFF      OFF     OFF      OFF
#
# 1260s      OFF      OFF     ON       OFF
#
# 1380s      ON       ON      ON       OFF

### 溫度到達停機排程 ###

#        電磁閥   壓縮機   風扇
#
# 0s      ON      OFF     ON
#
# 60s     OFF      OFF     ON
#
# 180s    ON       OFF     ON
#
# 220s    ON       ON      ON

import time
import sys
import signal
import threading as thr
import modbusHandler as mh
import RPiGPIO as rp
import database as db
import io_api as ia

# modbusHandeler.py: 讀壓縮機頻率、變頻器errorcode；寫壓縮機啟停、頻率
# RPiGPIO.py: 電磁閥(valve)、除霜電熱(heater)、警報(alarm)、風扇啟停(fan)
# database.py: mongoDB讀取DAG溫度


class FFControl(thr.thread):
    def __init__(self):
        thr.Thread.__init__(self)

        # configuration
        self.frequency = 60
        self.sys_interval = 5       # 系統執行頻率(seconds)

        self.targetT = -24         # 目標溫度(celsius)

        self.defrost_int = 14400     # 除霜間隔(seconds)
        self.defrost_time3 = 10      # 除霜開始後經過t1秒開啟除霜電熱
        self.defrost_heater = -50    # 溫度大於該值關閉除霜電熱
        self.defrost_time2 = 1260    # 除霜開始後經過t2秒後開啟風扇
        self.defrost_time3 = 1260    # 除霜開始後經過t3秒後開啟壓縮機電磁閥

        self.valve_inv = 180        # 電磁閥保護時間
        self.compressor_inv = 220  # 停機保護時間

        # initial timer
        self.compressor_start = 0   # 停機起始時間
        self.alive = 0              # 程式存活時間
        self.defrost_recover = 0    # 除霜復歸時間
        self.defrost_start = 0      # 除霜起始時間

        # sensor data
        # self.Fdoor = 0         # 開關門頻率，0沒開 1全開
        self.DAG = 0         # 回風溫度
        # self.Tenv = 0         # 環境溫度

        # CHT settings
        # self.sensorID = {"RAid": '10', "RDoorid": '12', "LDoorid": '13'}
        # self.data_len = self.HPC_interval/2   # 目前data logger每2秒讀一次

        # self.f_upper = 1           # 全載啟動flag
        # self.f_control = 0          # 熱負載預測flag
        self.f_target = 0           # 停機flag(0:溫度到停機; 1:製冷運轉中)
        self.f_defrost = 0          # 除霜狀態
        self.f_fan = 0              # 風扇轉速指令 0-> 3000rpm; 1-> 0rpm
        self.f_valve = 0
        self.f_heater = 0
        self.f_heaterRe = 0
        self.f_compressor = 0

    def updateSensor(self, DAGonly=False):
        if not DAGonly:
            # 開關門頻率
            # Fright, Fleft = self.readGPIO.calDoorFreq()
            # Fright = cht.Lookback_Data(
            #     self.sensorID["RDoorid"], self.data_len, 1)
            # Fleft = cht.Lookback_Data(
            #     self.sensorID["LDoorid"], self.data_len, 1)
            # if Fright != None and Fleft != None:
            # self.Fdoor = (Fright + Fleft)/2  # 改成0長閉、1常開（之前相反所以要1-value）
            # 環境溫度，目前實驗不影響(之後要改)
            self.Tenv = 0
        else:
            # 回風溫度
            # RA = cht.getData(self.sensorID["RAid"])
            # RA = self.readGPIO.readTemperGPIO()
            # 防止因max6675 腳位不穩導致數值亂跳
            # if not float(RA) < 0 or abs(float(self.TempRA) - float(RA)) > 4:
            #     self.TempRA = RA
            SA = db.queryDAG()
            if not SA == None:
                # self.TeDmpRA = float(RA) * 0.1
                self.DAG = SA
            # else:  # 保護防止CHT無法存取
            #     row = [ia.currentTime(), "ERROR: 讀取CHT平台失敗。",
            #            round(self.TempRA, 3)]
            #     ia.writeCsv(self.bound_log, row)
            #     print(ia.currentTime(), "ERROR", "讀取CHT平台失敗。")

    def defrostProcess(self):
        if self.f_defrost == 0:  # stage 1
            self.f_defrost = 1
            mh.writeEvent('compressor', 0)  # 關閉壓縮機
            rp.writeGPIO('fan', 0)  # 關閉風扇
            rp.writeGPIO('heater', 0)  # 關閉電熱除霜
            self.f_heater, self.f_fan = 0, 0
            self.defrost_start = time.time()
            msg = [ia.currentTime(), "啟動除霜。", round(self.DAG, 3)]
            ia.writeCsv(self.bound_log, msg)
            print(ia.currentTime(), "啟動除霜。")

        if self.f_defrost == 1:
            lasted = time.time() - self.defrost_start
            if lasted > 60 and lasted < 70:  # stage 2
                rp.writeGPIO('valve', 0)  # 關閉電磁閥
                self.f_valve = 0
                msg = [ia.currentTime(), "關閉電磁閥。", round(self.DAG, 3)]
                ia.writeCsv(self.bound_log, msg)
                print(ia.currentTime(), "關閉電磁閥。")
            elif lasted > 70:
                if self.f_heater == 0 and self.f_heaterRe == 0:  # stage 3
                    rp.writeGPIO('heater', 1)  # 開啟除霜電熱
                    msg = [ia.currentTime(), "開啟除霜電熱。", round(self.DAG, 3)]
                    ia.writeCsv(self.bound_log, msg)
                    print(ia.currentTime(), "開啟除霜電熱。")
                    self.f_heater = 1
                if (self.DAG > -50 or lasted > 1260) and self.f_heater == 1:  # stage 4
                    rp.writeGPIO('heater', 0)  # 關閉除霜電熱
                    msg = [ia.currentTime(), "關閉除霜電熱。",
                           round(self.DAG, 3)]
                    ia.writeCsv(self.bound_log, msg)
                    print(ia.currentTime(), "關閉除霜電熱。")
                    self.f_heater, self.f_heaterRe = 0, 1
                if lasted > 1260 and lasted < 1380 and self.f_fan == 0:  # stage 5
                    rp.writeGPIO('fan', 1)  # 開啟風扇
                    msg = [ia.currentTime(), "開啟風扇。",
                           round(self.DAG, 3)]
                    ia.writeCsv(self.bound_log, msg)
                    print(ia.currentTime(), "開啟風扇。")
                    self.f_fan = 1
                if lasted > 1380 and lasted < 1420 and self.f_valve == 0:  # stage 6
                    rp.writeGPIO('valve', 1)  # 開啟電磁閥
                    msg = [ia.currentTime(), "開啟電磁閥。",
                           round(self.DAG, 3)]
                    ia.writeCsv(self.bound_log, msg)
                    print(ia.currentTime(), "開啟電磁閥。")
                    self.f_valve = 1
                if lasted > 1420:  # stage 7
                    rp.writeGPIO('compressor', 1)  # 開啟壓縮機
                    mh.writeEvent('freq', self.frequency)  # 設定壓縮機轉速頻率
                    msg = [ia.currentTime(), "結束除霜，開啟壓縮機。", round(self.DAG, 3)]
                    ia.writeCsv(self.bound_log, msg)
                    print(ia.currentTime(), "結束除霜，開啟壓縮機。")
                    self.f_defrost, self.f_heaterRe, self.defrost_recover = 0, 0, time.time()

    def control(self):
        if self.DAG < -240 and self.f_target == 1:
            self.compressor_start = time.time()
            rp.writeGPIO('valve', 0)  # 關閉電磁閥
            rp.writeGPIO('compressor', 0)  # 關閉壓縮機
            msg = [ia.currentTime(), "溫度到達關閉電磁閥、壓縮機。", round(self.DAG, 3)]
            ia.writeCsv(self.bound_log, msg)
            print(ia.currentTime(), "溫度到達關閉電磁閥、壓縮機。")
            self.f_valve, self.f_target = 0, 0
        elif self.f_target == 0:
            lasted = time.time()-self.compressor_start
            if lasted > 180 and lasted < 220 and self.f_valve == 0:
                rp.writeGPIO('valve', 1)  # 開啟電磁閥
                msg = [ia.currentTime(), "開啟電磁閥。",
                       round(self.DAG, 3)]
                ia.writeCsv(self.bound_log, msg)
                print(ia.currentTime(), "開啟電磁閥。")
                self.f_valve = 1
            if lasted > 220 and self.f_target == 0:
                rp.writeGPIO('compressor', 1)  # 開啟壓縮機
                msg = [ia.currentTime(), "結束停機，開啟壓縮機。", round(self.DAG, 3)]
                ia.writeCsv(self.bound_log, msg)
                print(ia.currentTime(), "結束停機，開啟壓縮機。")
                self.f_target = 1
            if lasted > 240 and self.f_target == 1:
                mh.writeEvent('freq', self.frequency)  # 設定壓縮機轉速頻率
                msg = [ia.currentTime(), "設定壓縮機頻率。", round(self.DAG, 3)]
                ia.writeCsv(self.bound_log, msg)
                print(ia.currentTime(), "設定壓縮機頻率。")

    def run(self):
        sys_count = time.time()
        alg_count = sys_count
        signal.signal(signal.SIGINT, self.signal_handler)
        # self.printConfig()
        while (True):
            # 除霜機制
            if self.defrost_recover == 0:   # 實驗階段，程式剛執行時不先啟動除霜，之後可改。
                self.defrost_recover = time.time()
            if time.time() - self.defrost_recover > self.defrost_int:
                self.defrostProcess()
            # 溫度控制機制
            if (time.time() - sys_count) >= self.sys_interval:
                if self.f_defrost == 0:
                    sys_count = time.time()
                    # self.boundControl()
                else:
                    self.updateSensor(DAGonly=True)
            # # 熱負載預測控制演算法
            # if (time.time() - alg_count) >= self.HPC_interval and self.f_defrost == 0:
            #     alg_count = time.time()
            #     self.heatloadPredictionControl()
            # 系統存活判斷
            if (time.time() - self.alive) >= 10:
                # print(ia.currentTime(), " TemRA: {0:.3f}".format(self.DAG))
                print(".", end="")
                log = [ia.currentTime(), ia.currentTime(self.defrost_recover), self.defrost_int, self.f_defrost, round(self.DAG, 3), round(self.Fdoor, 4),
                       self.f_upper, self.f_control, self.f_target, self.serialObj.cpValue_old, self.serialObj.fanValue_old, self.f_fan, self.qPrev]
                ia.writeCsv(self.variables, log)
                self.alive = time.time()
            # terminal輸入修改風扇實驗模式
            if self.inp.status:
                # print("set self.serialObj.fanValue to", self.inp.command)
                self.serialObj.fanValue = str(int(self.inp.command)+2)
                self.inp.status = False

            time.sleep(0.5)
        print("Program terminated.")
        sys.exit(0)


if __name__ == '__main__':

    # database = db.mongoDB()
    # modbus = mh.inputCommand()
    # GPIO = rp.rpi()

    main = FFControl()
    main.daemon = True
    main.start()

    while True:
        time.sleep(1)
    # ser = cp.hzSerial()
    # HPC = HPCcontrol()
