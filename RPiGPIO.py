import RPi.GPIO as GPIO
import time

mode = 1  # 1: fanControl, 0: without control

EEV = {'name': 'alarm', 'pin': 7, 'status': False}
valve = {'name': 'valve', 'pin': 11, 'status': False}
fan = {'name': 'fan', 'pin': 13, 'status': False, 'freq': 0}
heater = {'name': 'heater', 'pin': 15, 'status': False}
lDoor = {'name': 'left_door', 'pin': 19, 'status': False}
rDoor = {'name': 'right_door', 'pin': 21, 'status': False}
f_defrost = False


GPIO.cleanup()
GPIO.setmode(GPIO.BOARD)

GPIO.setup(EEV['pin'], GPIO.OUT)
GPIO.setup(valve['pin'], GPIO.OUT)
GPIO.setup(heater['pin'], GPIO.OUT)
GPIO.setup(fan['pin'], GPIO.OUT)
GPIO.setup(lDoor['pin'], GPIO.IN)
GPIO.setup(rDoor['pin'], GPIO.IN)

GPIO.output(EEV['pin'], False)  # 關電子膨脹閥
GPIO.output(valve['pin'], False)  # 關電磁閥
GPIO.output(heater['pin'], False)  # 關電熱
GPIO.output(fan['pin'], False)  # 關風扇


def fanControl(chn):
    if not f_defrost:
        if GPIO.input(lDoor['pin']) and GPIO.input(rDoor['pin']):
            try:
                writeGPIO(fan, 1)
            except:
                print(f'Another process is using GPIO.')
        elif mode == 1:
            try:
                writeGPIO(fan, 0)
            except:
                print(f'Another process is using GPIO.')
    else:
        try:
            writeGPIO(fan, 0)
        except:
            print(f'Another process is using GPIO.')


def writeGPIO(unit: dict, value):
    if value == 0 and unit['status'] != False:
        unit['status'] = False
        GPIO.output(unit['pin'], False)
        return (True)
    elif value == 1 and unit['status'] != True:
        unit['status'] = True
        GPIO.output(unit['pin'], True)
        return (True)
    elif unit == fan and unit['freq'] != value:
        unit['freq'] = value
        GPIO.output(unit['pin'], unit['freq'])
        return (True)
    else:
        return (False)


GPIO.add_event_detect(lDoor['pin'], GPIO.BOTH,
                      callback=fanControl, bouncetime=200)
GPIO.add_event_detect(rDoor['pin'], GPIO.BOTH,
                      callback=fanControl, bouncetime=200)


# if __name__ == '__main__':
#    doorPin = 12
#    GPIO.cleanup()
#    GPIO.setmode(GPIO.BOARD)
#    GPIO.setup(doorPin=12, GPIO.IN, pull_up_down=GPIO.PUD_UP)
#
#    while True:
#        print(GPIO.input(doorPin))
#        time.sleep(0.5)
