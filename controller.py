import serial        # 引用pySerial模組
import time as t
import threading
import io_api as io
import sys


class sendSerial():
    def __init__(self):
        self.COM_PORT = '/dev/ttyACM0'    # 指定通訊埠名稱
        # self.COM_PORT = 'COM6'
        self.BAUD_RATES = 9600    # 設定傳輸速率
        self.ser = serial.Serial(self.COM_PORT, self.BAUD_RATES)
        self.cpValue_old = 0
        self.cpValue = 0
        self.fanValue_old = 0
        self.fanValue = 0
        self.type = "compressor"

        self.runThread = threading.Thread(target=self.sendCommand)
        self.runThread.daemon = True
        self.runThread.start()
        self.errorCount = 0

    def sendCommand(self):
        while True:
            try:
                if self.cpValue != self.cpValue_old:
                    if self.cpValue > 2:
                        print(io.currentTime(), "send {0}hz to driver".format(
                            str(self.cpValue)))
                    self.ser.write(
                        bytes('c'+str(self.cpValue)+'\n', 'utf-8'))
                    self.cpValue_old = self.cpValue

                elif self.fanValue != self.fanValue_old:
                    print(io.currentTime(), "send command {0} to fan".format(
                        str(self.fanValue)))
                    self.ser.write(bytes('f'+str(self.fanValue)+'\n', 'utf-8'))
                    # self.ser.write(bytes('f'+str(self.fanValue), 'utf-8'))
                    self.fanValue_old = self.fanValue

                while self.ser.in_waiting:          # 若收到序列資料…
                    ser_data_raw = self.ser.readline()  # 讀取一行
                    ser_data = ser_data_raw.decode()   # 用預設的UTF-8解碼
                    print(io.currentTime(), ser_data)

                self.errorCount = 0
                t.sleep(5)

            except Exception as e:
                print(io.currentTime(), "ERROR when sendSerial.", e)
                if self.errorCount < 5:
                    self.errorCount += 1
                else:
                    print("Please restart arduino to initialize system.")
                    return

    def exitProgram(self):
        sys.exit(0)


if __name__ == '__main__':
    test = sendSerial()
    t.sleep(10)
    test.fanValue = '3'
    while True:
        t.sleep(1)
