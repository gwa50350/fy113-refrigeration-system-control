import serial        # 引用pySerial模組
import time as t
import threading
import io_api as io


class hzSerial():
    def __init__(self):
        self.COM_PORT = '/dev/ttyACM0'    # 指定通訊埠名稱
        self.BAUD_RATES = 9600    # 設定傳輸速率
        self.ser = serial.Serial(self.COM_PORT, self.BAUD_RATES)   # 初始化序列通訊埠
        self.controlValue_old = 0
        self.controlValue = 0

        self.runThread = threading.Thread(target=self.hzSetting)
        self.runThread.daemon = True
        self.runThread.start()

    def hzSetting(self):
        while True:
            try:
                if self.controlValue != self.controlValue_old:
                    if self.controlValue > 2:
                        print(io.currentTime(), "send {0}hz to driver".format(
                            str(self.controlValue)))
                    self.ser.write(bytes(str(self.controlValue)+'\n', 'utf-8'))
                    self.controlValue_old = self.controlValue

                while self.ser.in_waiting:          # 若收到序列資料…
                    ser_data_raw = self.ser.readline()  # 讀取一行
                    ser_data = ser_data_raw.decode()   # 用預設的UTF-8解碼
                    print(io.currentTime(), '[Compressor Arduino]:', ser_data)

                t.sleep(5)

            except Exception as e:
                print(io.currentTime(), "ERROR in compressor controling", e)


"""
#60Hz  -> 1800rpm
#66Hz  -> 2000rpm
#94Hz  -> 2800rpm
#100Hz -> 3000rpm
#120Hz -> 3600rpm
#150Hz -> 4500rpm
"""
