#!/usr/bin/python
# -*- coding: utf-8 -*-
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# ITRI
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# Copyright (c) 2022, ITRI
# All rights reserved.
# Use of this source code is governed by a BSD-style license
# that can be found in the LICENSE file.
#
# Download RAG Temp Data from cht iot api
#
# Author : miku
# Date   : 25/3/2023

'''
[任務:]讀取中華電信IoT平台資料
step1: 讀取中華電信IoT
step2: 計算多次平均

[提醒!!]工研院智障網路長城:
import certifi
certifi.where()
以Windows平台為例，請執行如下步驟：
1.下載附件auto_ITRIRoot256.tar並解壓縮
2.執行pip install certifi
3.執行python
4.於>>>提示字元下執行如下:
5.import certifi
6.certifi.where()
7.上述指令顯示python的cacert.pem路徑位置
8.exit()
9.將步驟1解開後的ITRIroot256.pem加至cacert.pem，做法如下：
ITRIroot256.pem >> python的cacert.pem路徑位置

[提醒!!]中華電信IoT智慧聯網大平台: https://iot.cht.com.tw/iot/
每一個金鑰10秒內只允許 Read (GET) 100次與 Write (POST, PUT) 200次，
若超過限制，會收到406 Not Acceptable 錯誤訊息

'''

import requests
import json
import time
import datetime
import io_api as ia


# 中華電信請求發送太多次會擋，前面需加這個較佳
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

### =======<中華電信設定>======= ###
deviceID = '30955741961'
key = 'PKFRZYPFBAWHBPH5XT'
headers = {"CK": key, "accept": "application/json"}


def Lookback_Data(ID, Lookback_Num, functioncode=0):  # (MW100對應ID, 回推次數, 是否開關門計算)
    try:
        # [step1: 讀取中華電信IoT]
        try:
            timedelta_minutes = int(Lookback_Num*2/60)  # 向下取整數,加個保險而已
            # print('timedelta_minutes:',timedelta_minutes)
            Strat_Time = (datetime.datetime.now()+datetime.timedelta(hours=-8,
                          minutes=-timedelta_minutes)).strftime('%Y-%m-%dT%H:%M:%S.000Z')
            End_Time = (datetime.datetime.now(
            )+datetime.timedelta(hours=-8)).strftime('%Y-%m-%dT%H:%M:%S.000Z')

            apiURL = 'https://iot.cht.com.tw/iot/v1/device/' + deviceID + \
                '/sensor/' + ID + '/rawdata?start='+Strat_Time+'&end='+End_Time
            response = requests.get(apiURL, headers=headers, verify=False)
            data_dic = json.loads(response.text)
            # RAG_Now = int(data_dic['value'][0])
        except Exception as e:
            print(e)

        # [step2: 計算多次平均]
        nums = list()  # None
        for i in data_dic:
            # print(int(i['value'][0]))
            nums.append(int(i['value'][0]))
        if len(nums) == 0:
            print("Warning: no door status data from cht. Is cloud crash?")
        nums_mean = sum(nums)/float(len(nums))
        # print(RAG_mean)
        if functioncode == 1:  # [開關門計算]
            nums_max = 0.0001 if max(nums) == 0 else max(nums)
            nums_mean = nums_mean/nums_max
            return 0 if nums_mean < 0.0001 else nums_mean
        else:  # [一般計算]
            return 0 if nums_mean < 0.0001 else nums_mean

    except Exception as e:
        print(e)


def getData(ID, Counter=1):
    if Counter > 3:
        print('<< get data from CHT failed, leaving... >>\n')
        return None
    try:
        apiURL = 'https://iot.cht.com.tw/iot/v1/device/' + \
            deviceID + '/sensor/' + ID + '/rawdata'
        response = requests.get(apiURL, headers=headers, verify=False)
        data_dic = json.loads(response.text)
        val = int(data_dic['value'][0])
        return val
    except Exception as e:
        print(e)
        print(time.strftime("%Y-%m-%d  %H:%M:%S", time.localtime()),
              "Error when get data ID:{0}. ".format(ID), Counter)
        time.sleep(3)
        data = getData(ID, Counter+1)
        return data


if __name__ == '__main__':
    while True:
        RAG_mean = Lookback_Data('10', 90)  # (MW100對應ID, 回推次數)
        # (MW100對應ID, 回推次數, 是否開關門計算)
        Door_Left_mean = Lookback_Data('12', 90, 1)
        Door_Right_mean = Lookback_Data('13', 90, 1)
        time.sleep(5)

    # print(Lookback_Data('13', 90, 1))
