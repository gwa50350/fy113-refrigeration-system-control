import compressor as cp
import fan
import inputCom as com
import readingCHT as cht
import calHeatLoad as hl
import io_api as ia
import time
import threading as thr


class HPCcontrol(thr.Thread):
    def __init__(self, cpObject, fanObject, inp):
        thr.Thread.__init__(self)
        self.cpObject = cpObject
        self.fanObject = fanObject
        self.inp = inp

        # configuration
        self.HPC_interval = 180     # 熱負載預測控制執行頻率(seconds)
        self.sys_interval = 5       # 系統執行頻率(seconds)
        self.capacity = 700       # 壓縮機冷凍能力(W)

        self.targetT = 3         # 目標溫度(celsius)
        self.thres1 = 5         # 啟動熱負載預測控制門檻(celsius)
        self.thres2 = 9         # 啟動強制全載門檻(celsius)
        self.qPrev = 0         # 前次未滿足的heatload，初始為0

        self.defrost_int = 14400     # 除霜間隔(seconds)
        self.defrost_dur = 600       # 除霜長度(seconds)
        self.defrost_pro = 120       # 除霜保護(seconds)

        self.compressor_inv = 530  # 停機保護時間
        self.valve_inv = 180        # 電磁閥保護時間

        # initial timer
        self.compressor_start = 0   # 停機起始時間
        self.alive = 0              # 程式存活時間
        self.defrost_recover = 0    # 除霜復歸時間
        self.defrost_start = 0      # 除霜起始時間

        # sensor data
        self.Fdoor = 0         # 開關門頻率，0沒開 1全開
        self.TempRA = 0         # 回風溫度
        self.Tenv = 0         # 環境溫度

        # CHT settings
        self.sensorID = {"RAid": '10', "RDoorid": '12', "LDoorid": '13'}
        self.data_len = self.HPC_interval/2   # 目前data logger每2秒讀一次

        self.f_upper = 0           # 全載啟動flag
        self.f_control = 1          # 熱負載預測flag
        self.f_target = 0           # 停機flag
        self.f_defrost = 0          # 除霜狀態
        # self.f_fan = 0              # 風扇轉速指令 0-> 3000rpm; 1-> 0rpm

        # output path
        self.heatload_log = "output/heatload_log.csv"
        self.bound_log = "output/bound_log.csv"
        self.variables = "output/variables_log.csv"

        # start
        msg = [ia.currentTime(), "控制系統啟動。", None]
        ia.writeCsv(self.bound_log, msg)
        print(ia.currentTime(), "控制系統啟動。")
        self.runHPCcontrol()

    def updateSensor(self, RAonly=False):
        if not RAonly:
            # 開關門頻率
            Fright = cht.Lookback_Data(
                self.sensorID["RDoorid"], self.data_len, 1)
            Fleft = cht.Lookback_Data(
                self.sensorID["LDoorid"], self.data_len, 1)
            if Fright != None and Fleft != None:
                self.Fdoor = 1 - (Fright + Fleft)/2  # 保護防止CHT無法存取
        # 環境溫度，目前實驗不影響(之後要改)
            self.Tenv = 0
        else:
            # 回風溫度
            RA = cht.getData(self.sensorID["RAid"])
            if not RA == None:
                self.TempRA = RA*0.1
            else:  # 保護防止CHT無法存取
                row = [ia.currentTime(), "ERROR: 讀取CHT平台失敗。",
                       round(self.TempRA, 3)]
                ia.writeCsv(self.bound_log, row)
                print(ia.currentTime(), "ERROR", "讀取CHT平台失敗。")

    def evalCapacity(self, Qpre, Qnow):  # 計算是否超過冷凍能力
        Qpre += Qnow
        if Qpre > self.capacity:
            Qpre -= self.capacity
            Qnow = self.capacity
        else:
            Qpre, Qnow = 0, Qpre

        return Qpre, Qnow

    def heatloadPredictionControl(self):
        self.updateSensor()

        # qLoad = hl.accuHeatLoad(Fdoor, Tenv, interval)
        self.tempEnv = 23  # 實驗用，進行其他環境溫度實驗室要改
        if self.Fdoor < 0.0001:
            qLoad = hl.Q23(0.0001)
        else:
            qLoad = hl.Q23(self.Fdoor)
        # qLoad = hl.Q26(Fdoor)
        # qLoad = hl.Q29(Fdoor)
        if self.f_control == 1:                         # 表示回風溫度到達啟動控制
            if self.f_target == 1 or self.f_upper == 1:     # 表示曾發生全載或停機event => qPrev歸零
                self.f_upper, self.f_control, self.f_target = 0, 1, 0
                self.qPrev = 0
            if (time.time()-self.compressor_start) < (self.compressor_inv + self.valve_inv):
                self.qPrev += qLoad
            else:
                self.qPrev, qLoad = self.evalCapacity(self.qPrev, qLoad)
                # calculate Hz
                hz = hl.QtoHz(qLoad)  # 30hz - 75hz
                # set Hz to BLDC
                self.cpObject.controlValue = int(hz)
            msg = [ia.currentTime(), round(self.tempEnv, 2), round(self.Fdoor, 4), round(
                self.TempRA, 3), round(qLoad, 1), round(self.qPrev, 1), hz]
            ia.writeCsv(self.heatload_log, msg)

    def boundControl(self):
        self.updateSensor(RAonly=True)

        if (self.f_upper and self.TempRA < self.thres1) or (self.f_target and self.TempRA > self.thres1):
            if self.f_control == 0:
                msg = [ia.currentTime(), "達到控制溫度，啟動熱負載預測控制。",
                       round(self.TempRA, 3)]
                ia.writeCsv(self.bound_log, msg)
                print(ia.currentTime(), "達到控制溫度，啟動熱負載預測控制。")
                self.f_control = 1

        delay = time.time()-self.compressor_start

        if delay < (self.compressor_inv + self.valve_inv):
            if delay >= (self.valve_inv) and self.cpObject.controlValue_old == 0:
                msg = [ia.currentTime(), "開啟電磁閥。", round(self.TempRA, 3)]
                ia.writeCsv(self.bound_log, msg)
                print(ia.currentTime(), "開啟電磁閥。")
                self.cpObject.controlValue = 1
                self.fanObject.controlValue = 1 if self.f_defrost == 0 else 0

            if self.TempRA <= self.targetT and self.f_target == 0:
                self.f_upper, self.f_control, self.f_target = 0, 0, 1

        # if delay >= (self.valve_inv) and delay < (self.compressor_inv + self.valve_inv):
        #     if self.cpObject.controlValue_old == 0:
        #         msg = [self.currentTime(), "開啟電磁閥。", round(self.TempRA,3)]
        #         ia.writeCsv(self.bound_log, msg)
        #         self.cpObject.controlValue = 1

        else:
            self.fanObject.controlValue = 0
            if self.TempRA >= self.thres2 and self.f_upper == 0:
                msg = [ia.currentTime(), "達到溫度上限，壓縮機全載。",
                       round(self.TempRA, 3)]
                ia.writeCsv(self.bound_log, msg)
                print(ia.currentTime(), "達到溫度上限，壓縮機全載。")
                self.f_upper, self.f_control, self.f_target = 1, 0, 0
                self.cpObject.controlValue = 150

            elif self.TempRA <= self.targetT and self.f_target == 0:
                msg = [ia.currentTime(), "達到目標溫度，壓縮機停機。",
                       round(self.TempRA, 3)]
                ia.writeCsv(self.bound_log, msg)
                print(ia.currentTime(), "達到目標溫度，壓縮機停機。")
                self.f_upper, self.f_control, self.f_target = 0, 0, 1
                self.compressor_start = time.time()   # 紀錄保護機制起始時間
                self.cpObject.controlValue = 0
                self.fanObject.controlValue = 0

    def defrostProcess(self):
        if self.f_defrost == 0:
            self.f_defrost = 1
            self.cpObject.controlValue = 0
            # self.fanObject.controlValue = 1
            self.defrost_start = time.time()
            msg = [ia.currentTime(), "啟動除霜。", round(self.TempRA, 3)]
            ia.writeCsv(self.bound_log, msg)
            print(ia.currentTime(), "啟動除霜。")

        if self.f_defrost == 1:
            lasted = time.time() - self.defrost_start
            if lasted > self.defrost_pro and lasted < self.defrost_dur:   # 除霜2分鐘後開啟電磁閥跟風扇
                self.cpObject.controlValue = 50         # 給壓縮機轉速50hz，驅動器會開啟風扇但壓縮機頻率低於60hz不會啟動
            if lasted > self.defrost_dur:
                msg = [ia.currentTime(), "結束除霜。", round(self.TempRA, 3)]
                ia.writeCsv(self.bound_log, msg)
                print(ia.currentTime(), "結束除霜。")
                # self.fanObject.controlValue = 0
                self.f_defrost, self.defrost_recover = 0, time.time()
                self.f_upper, self.f_control, self.f_target = 0, 1, 0

    def runHPCcontrol(self):
        sys_count = time.time()
        alg_count = sys_count
        while (True):
            # 除霜機制
            if self.defrost_recover == 0:   # 實驗階段，程式剛執行時不先啟動除霜，之後可改。
                self.defrost_recover = time.time()
            if time.time() - self.defrost_recover > self.defrost_int:
                self.defrostProcess()
            # 溫度上下限控制機制
            if (time.time() - sys_count) >= self.sys_interval and self.f_defrost == 0:
                sys_count = time.time()
                self.boundControl()
            # 熱負載預測控制演算法
            if (time.time() - alg_count) >= self.HPC_interval and self.f_defrost == 0:
                alg_count = time.time()
                self.heatloadPredictionControl()
            # 系統存活判斷
            if (time.time() - self.alive) >= 10:
                # print(ia.currentTime(), " TemRA: {0:.3f}".format(self.TempRA))
                print(".", end="")
                log = [ia.currentTime(), ia.currentTime(self.defrost_recover), self.defrost_int, self.f_defrost, round(self.TempRA, 3), round(self.Fdoor, 4),
                       self.f_upper, self.f_control, self.f_target, self.cpObject.controlValue_old, self.fanObject.controlValue_old, self.qPrev]
                ia.writeCsv(self.variables, log)
                self.alive = time.time()
            # terminal輸入修改風扇實驗模式
            if self.inp.status:
                print("set self.fanObject.controlValue to", self.inp.command)
                self.fanObject.controlValue = str(int(self.inp.command)+2)
                self.inp.status = False

            time.sleep(0.5)


if __name__ == '__main__':

    cpControl = cp.hzSerial()
    fanControl = fan.ecFan()
    inp = com.inputCommand()

    main = HPCcontrol(cpControl, fanControl, inp)
    main.daemon = True
    main.start()

    while True:
        time.sleep(1)
    # ser = cp.hzSerial()
    # HPC = HPCcontrol()
