import serial        # 引用pySerial模組
import time as t
import threading
import io_api as ia


class ecFan():
    def __init__(self):
        self.COM_PORT = '/dev/ttyACM1'    # 指定通訊埠名稱
        self.BAUD_RATES = 9600    # 設定傳輸速率
        self.ser = serial.Serial(self.COM_PORT, self.BAUD_RATES)   # 初始化序列通訊埠
        self.controlValue_old = 0
        self.controlValue = 0

        self.runThread = threading.Thread(target=self.hzSetting)
        self.runThread.daemon = True
        self.runThread.start()

    def hzSetting(self):
        while True:
            try:
                if self.controlValue != self.controlValue_old:
                    self.ser.write(bytes(str(self.controlValue)+'\n', 'utf-8'))
                    self.controlValue_old = self.controlValue

                while self.ser.in_waiting:          # 若收到序列資料…
                    ser_data_raw = self.ser.readline()  # 讀取一行
                    ser_data = ser_data_raw.decode()   # 用預設的UTF-8解碼
                    print(ia.currentTime(), '[Fan Arduino]:', ser_data)

                t.sleep(5)

            except Exception as e:
                print(ia.currentTime(), "ERROR in fan controling", e)


""" 
controlValue    | PWM   | hz    | rpm
 0                0%      0       0
 1                10%     36.66   1100
 2                30%     50      1500
 3                50%     66.66   2000
 4                70%     85      2550
 5                90%     100     3000
"""
