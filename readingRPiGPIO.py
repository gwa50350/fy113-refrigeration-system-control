#!/usr/bin/python
# -*- coding: utf-8 -*-
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# ITRI
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# Copyright (c) 2022, ITRI
# All rights reserved.
# Use of this source code is governed by a BSD-style license
# that can be found in the LICENSE file.
#
# Download RAG Temp Data from cht iot api
#
# Author : miku
# Date   : 23/11/2023

'''
[任務:]讀取GPIO
step1: 讀取GPIO
step2: 計算多次平均
'''

import time
import datetime
import io_api as i
import RPi.GPIO as GPIO
import threading
import max6675


class readingGPIO():
    def __init__(self):
        self.Lookback_Max = 180  # 熱負載預測180sec執行1次
        self.Read_TimeDelay = 1

        # GPIO configuration
        GPIO.setmode(GPIO.BOARD)  # 使用板上定義的腳位號碼
        self.Left_DOOR_Pin = 18
        self.Right_DOOR_Pin = 16

        self.tem_cs = 11
        self.tem_sck = 13
        self.tem_so = 15

        GPIO.setup(self.Left_DOOR_Pin, GPIO.IN)
        GPIO.setup(self.Right_DOOR_Pin, GPIO.IN)
        max6675.set_pin(self.tem_cs, self.tem_sck, self.tem_so, 1)

        self.Left_DOOR_History = [0.0]*self.Lookback_Max
        self.Right_DOOR_History = [0.0]*self.Lookback_Max

        self.runThread = threading.Thread(target=self.readDoorGPIO)
        self.runThread.daemon = True
        self.runThread.start()

    # def calDoorStatus(self):
    #     print("判斷並輸出開關門狀態")

    def calDoorFreq(self):
        Door_Left_mean = self.Lookback_Data(self.Left_DOOR_History, 1)
        Door_Right_mean = self.Lookback_Data(self.Right_DOOR_History, 1)
        print("Door frequency, left: ", str(Door_Left_mean),
              ", right: ", str(Door_Right_mean))
        return Door_Left_mean, Door_Right_mean

    def readDoorGPIO(self):
        while True:
            try:
                Left_DOOR_Pin_STATUS = GPIO.input(self.Left_DOOR_Pin)
                Right_DOOR_Pin_STATUS = GPIO.input(self.Right_DOOR_Pin)
                self.Left_DOOR_History.append(Left_DOOR_Pin_STATUS)  # 新值加入後
                self.Left_DOOR_History.pop(0)
                self.Right_DOOR_History.append(Right_DOOR_Pin_STATUS)
                self.Right_DOOR_History.pop(0)
            except Exception as e:
                print(e)
                print("reading door GPIO failed")

            time.sleep(self.Read_TimeDelay)

    def readTemperGPIO(self):
        value = max6675.read_temp(self.tem_cs)
        print("return air temperature: ", value)
        return value

    def Lookback_Data(self, Lookback_Num, functioncode=0):  # (回推次數, 是否開關門計算)
        try:
            nums_mean = sum(Lookback_Num)/float(self.Lookback_Max)
            # print(RAG_mean)
            if functioncode == 1:  # [開關門計算]
                nums_max = 0.0001 if max(
                    Lookback_Num) == 0 else max(Lookback_Num)
                nums_mean = nums_mean/nums_max
                return 0 if nums_mean < 0.0001 else nums_mean
            else:  # [一般計算]
                return 0 if nums_mean < 0.0001 else nums_mean

        except Exception as e:
            print(e)


# if __name__ == '__main__':
    # Left_DOOR_History = list()
    # Right_DOOR_History = list()
    # while True:
    #     # [step1: 讀取GPIO]
    #     try:
    #         Left_DOOR_Pin_STATUS = GPIO.input(Left_DOOR_Pin)
    #         Right_DOOR_Pin_STATUS = GPIO.input(Right_DOOR_Pin)
    #         Left_DOOR_History.append(Left_DOOR_Pin_STATUS)  # 新值加入後
    #         Right_DOOR_History.append(Right_DOOR_Pin_STATUS)
    #         if len(Left_DOOR_History) > Lookback_Number:  # 如刪除
    #             Left_DOOR_History.pop(0)
    #         if len(Right_DOOR_History) > Lookback_Number:
    #             Right_DOOR_History.pop(0)

    #         Door_Left_mean = Lookback_Data(Left_DOOR_History, 1)
    #         Door_Right_mean = Lookback_Data(Right_DOOR_History, 1)

    #     except Exception as e:
    #         print(e)
    #         print("GPIO掛掉!")

    #     RAG_mean = Lookback_Data('10', 90)  # (MW100對應ID, 回推次數)
    #     time.sleep(Read_TimeDelay)
