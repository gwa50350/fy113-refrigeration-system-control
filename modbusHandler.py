# 溫控器溫度設定
import serial
import time
import minimalmodbus
import threading


class mbHandler():
    def __init__(self):

        # RS485-Modbus
        self.Serial_port = '/dev/ttyUSB0'  # '/dev/ttyAMA0'
        self.baudrate = 9600  # 鮑率
        self.Address485 = 1  # 站台位置
        self.Delay_time_485 = 0.2

        self.status_write_addr = 0x2800
        self.status_read_addr = 0x2101
        self.freq_write_addr = 0x2801
        self.freq_read_addr = 0x2103
        self.freq_comm_addr = 0x2102
        self.error_addr = 0x2100

        self.freq = 0
        self.status = 0
        # self.status_new = 0
        self.error = 0
        self.freq_comm = 0
        self.freq_new = 0
        self.f_status = 0
        self.status_read = 0

        self.runThread = threading.Thread(target=self.run)
        self.runThread.daemon = True
        self.runThread.start()

    def run(self):
        timer = time.time()
        while (True):
            if not self.freq_comm == self.freq_new:
                self.writeEvent('freq', int(self.freq_new))
                self.readEvent('freq_comm')

            if self.f_status:
                self.writeEvent('compressor', int(self.status))
                self.f_status = 0

            if (time.time()-timer > 3):
                timer = time.time()
                self.readEvent('freq')
                self.readEvent('error')
                self.readEvent('status')
                # print (self.freq, self.freq_comm, self.freq_new, self.error, self.status, hex(self.status_read))

    def readEvent(self, var):
        if var == 'freq':  # 目前頻率
            value = self.transmit(self.freq_read_addr, 0, 3)
            if not value == None:
                self.freq = int(value/100)
                return self.freq
        elif var == 'error':  # 變頻器error code
            value = self.transmit(self.error_addr, 0, 3, length=1)
            if not value == None:
                self.error = value
                return self.error
        elif var == 'freq_comm':
            value = self.transmit(self.freq_comm_addr, 0, 3)
            if not value == None:
                self.freq_comm = int(value/100)
                return self.freq_comm
        elif var == 'status':
            value = self.transmit(self.status_read_addr, 0, 3)
            if not value == None:
                self.status_read = value
                return self.status_read

    def writeEvent(self, var, value485):
        if var == 'freq':  # 設定轉速頻率(hz)
            value = self.transmit(self.freq_write_addr, int(value485*100), 6)
            return value
        # 開啟/關閉壓縮機 (0: 關閉; 1: 開啟)
        elif var == 'compressor':
            if value485 == 0:
                self.transmit(self.status_write_addr, 0x0000, 6)
                print("關閉壓縮機")
                self.freq_new = 0
            elif value485 == 1:
                self.transmit(self.status_write_addr, 0x0001, 6)
                print("開啟壓縮機")

    def transmit(self, regaddr, value485, func, length=2):

        instrument = minimalmodbus.Instrument(
            port=self.Serial_port, slaveaddress=self.Address485, mode='rtu')
        instrument.serial.baudrate = self.baudrate
        instrument.serial.bytesize = 8
        # instrument.serial.parity = serial.PARITY_NONE
        instrument.serial.stopbits = 1
        instrument.serial.timeout = self.Delay_time_485
    # instrument.mode = minimalmodbus.MODE_RTU

        if func == 6:  # write
            try:
                value = instrument.write_register(
                    registeraddress=regaddr, value=value485, functioncode=func)  # (位置)
            except:
                return None

        elif func == 3:  # read
            try:
                value = instrument.read_register(regaddr, 0, func)  # (位置)
            except:
                return None
        instrument.serial.close()
        return value


if __name__ == '__main__':
    ff = mbHandler()
    instrument = minimalmodbus.Instrument(
        port=ff.Serial_port, slaveaddress=ff.Address485, mode='rtu')
    instrument.read_register(0x2103, 0, 3)
