
import time
import sys
import requests
import json
import csv
import time as t

# def readJson(path, Counter=1):
#     if Counter > 3:
#         print('<< read JSON file failed, leaving... >>\n')
#         sys.exit(0)
#     try:
#         with open(path, newline='') as f:
#             data = json.load(f)
#             f.close()
#             return data
#     except Exception as e:
#         errorPub(e, "Error when reading JSON file ", Counter)
#         t.sleep(10)
#         data = readJson(path,Counter+1)
#         return data

# def writeJson(path, data):
#     try:
#         with open(path, 'w') as f:
#             json.dump(data, f)
#             f.close()
#         return
#     except Exception as e:
#         errorPub(e, "Error when writing JSON file.")

# def readCsv(path, Counter=1):
#     if Counter > 3:
#         print('<< read CSV file failed, leaving... >>\n')
#         sys.exit(0)
#     try:
#         with open(path, 'r', newline='') as f:
#             rows = csv.reader(f, delimiter=',')
#             data = [row for row in rows]
#             f.close()
#             return data
#     except Exception as e:
#         errorPub(e, "Error when reading CSV file ", Counter)
#         t.sleep(10)
#         data = readJson(path,Counter+1)
#         return data


def writeCsv(path, event, mode='a+'):
    event.insert(0, currentTime())
    try:
        with open(path, mode, newline='', encoding='utf-8') as f:
            writer = csv.writer(f, delimiter=',')
            writer.writerow(event)
            f.close()
            print(*event)
        return
    except Exception as e:
        print(currentTime(), "ERROR when writing CSV file. " + e)


def writelog(path, event, mode='a+'):
    event.insert(0, currentTime())
    try:
        with open(path, mode, newline='', encoding='utf-8') as f:
            writer = csv.writer(f, delimiter=',')
            writer.writerow(event)
            f.close()
            # print(*event)
        print(f"{event[0]}, 除霜復歸:{event[1]}, 運轉:{event[2]}, 回風:{event[3]:{5}}, 出風:{event[4]:{5}}, 開關門:{event[5]:{7}}, 頻率:{event[6]:{3}}, 錯誤碼:{event[7]:{2}}")
        return
    except Exception as e:
        print(currentTime(), "ERROR when writing CSV file. " + e)

# def msgPrint(isError, message):
#     if isError:
#         print('ERROR ', t.strftime("%Y-%m-%d  %H:%M:%S", t.localtime()), message)
#     else:
#         print(t.strftime("%Y-%m-%d  %H:%M:%S", t.localtime()), message)


def currentTime(ts=None):
    if ts == None:
        return t.strftime("%m-%d %H:%M", t.localtime())
    else:
        return t.strftime("%H:%M", t.localtime(ts))
