import time as t
import threading

# 設定風扇控制模式，command = 1 為開關門啟停風扇；0為不控制風扇


class inputCommand():
    def __init__(self):
        self.command_old = 1
        self.command = 1
        self.f_defrost = False
        self.status = False
        self.f_shutdown = False

        self.runThread = threading.Thread(target=self.waitInput)
        self.runThread.daemon = True
        self.runThread.start()

    def waitInput(self):
        while True:
            self.command = int(input())
            if self.command == 0 or self.command == 1:
                if self.command != self.command_old:
                    print("set fan control mode to:", self.command)
                    self.command_old = self.command
                    self.status = True
            elif self.command == 2:
                self.f_defrost = True
                print(f"defrost will start immediately")
            elif self.command == 99:
                self.f_shutdown = True

            else:
                print(f"Wrong input, please enter 0 to 2 for command below:\n"
                      f"0: turn off fan control\n"
                      f"1: turn on fan control\n"
                      f"2: manually defrost\n"
                      f"99: shutdown")


if __name__ == '__main__':
    test = inputCommand()
    test.serialObj.fanValue = '3'
    while True:
        t.sleep(1)
