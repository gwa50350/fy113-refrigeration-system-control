import argparse
import time
import sys
# import signal
import threading as thr
# import inputCom as com
import calHeatLoad as hl
import io_api as ia
# import controller as con
import modbusHandler
# import database as db
import RPiGPIO as rp
import inputCom as ic
import pymongo

### 除霜排程 ###

#               電磁閥   壓縮機   風扇     除霜電熱
#
# 0s             ON       OFF     OFF      OFF
#
# 60s            OFF      OFF     OFF      OFF
#
# 70s            OFF      OFF     OFF      ON
#
# RAG>5 & 10s    OFF      OFF     OFF      OFF
#
# 1260s          OFF      OFF     ON       OFF
#
# 1380s          ON       OFF     ON       OFF
#
# 1420s          ON       ON      ON       OFF

### 溫度到達停機排程 ###

#        電磁閥   壓縮機   風扇
#
# 0s      ON      OFF     ON
#
# 60s     OFF      OFF     ON
#
# 180s    ON       OFF     ON
#
# 220s    ON       ON      ON


class HPCcontrol(thr.Thread):
    def __init__(self, modbus, inputCom, recover=0):
        thr.Thread.__init__(self)
        # self.mongo = mongo
        self.mh = modbus
        self.ic = inputCom

        # configuration

        self.HPC_interval = 180     # 熱負載預測控制執行頻率(seconds)
        self.sys_interval = 5       # 系統執行頻率(seconds)
        self.capacity = 961         # 壓縮機冷凍能力(W)

        self.targetTemp = -20        # 目標溫度(celsius)，啟動熱負載預測控制溫度
        self.controlTemp = -18          # 啟動熱負載預測控制門檻(celsius)
        self.upperTemp = -16          # 啟動強制全載門檻(celsius)
        self.qPrev = 0              # 前次未滿足的heatload，初始為0

        self.defrost_int = 14400    # 除霜間隔(seconds)
        self.defrost_t1 = 30        # 除霜開始後經過t1秒關閉valve
        self.defrost_t2 = 40        # 除霜開始後經過t2秒開啟heater
        self.defrost_heater = 5     # 溫度大於該值關閉電熱
        self.defrost_t3 = 1260      # 除霜開始後經過t3秒後開啟fan, valve, EEV
        self.defrost_t4 = 1380      # 除霜開始後經過t4秒後開啟壓縮機，結束除霜

        # database configuration
        self.url = "mongodb://web:02750963@140.96.8.136:27017/"
        self.client = pymongo.MongoClient(self.url)
        self.mydb_Name = "itri"
        self.mydb = self.client[self.mydb_Name]
        self.mycol = self.mydb["MW100"]

        # initial timer
        self.alive = 0              # 程式存活時間
        self.defrost_recover = recover if not recover == None else 0    # 除霜復歸時間
        self.defrost_start = 0      # 除霜起始時間
        # self.RAG_timer = 0          # 除霜時回風溫度>defrost_heater的起始時間
        self.timer = time.time()
        self.temp_timer = time.time()

        # sensor data
        self._RAG = 0                # 回風溫度
        self._DAG = 0
        self.RAG_avg30 = 0
        self.DAG_min10 = 0
        self.old_RAG = 0            # 前次回風溫度，用來判斷是否啟動熱負載預測控制
        self.Tenv = 0               # 環境溫度
        self.leftDoor = 0.0
        self.rightDoor = 0.0
        self.avgDoor = 0.0

        self._status = 0            # 運轉狀態，0:停機啟動;1:保護;2:運轉;3:除霜
        self._load = 0              # 變頻模式，0:低載;1:HPC;2:全載

        # CHT settings
        # self.sensorID = {"RAid": '10', "RDoorid": '12', "LDoorid": '13'}
        # self.data_len = self.HPC_interval/2   # 目前data logger每2秒讀一次

        self.f_defrost = 0          # 除霜狀態
        self.f_valve = 0
        self.f_heater = 0
        self.f_heaterRe = 0

        # output path
        self.heatload_log = "output/heatload_log.csv"
        self.bound_log = "output/bound_log.csv"
        self.variables = "output/variables_log.csv"

        # start
        msg = ["控制系統啟動。", None]
        ia.writeCsv(self.bound_log, msg)
        self.runHPCcontrol()

    # def signal_handler(self, signal, frame):
    #     print("Ctrl C is triggered.")
    #     # self.Shutdown()
    #     sys.exit(0)

    def __exit__(self, exc_type, exc_value, exc_traceback):
        print(exc_type)
        print(exc_value)
        print(exc_traceback)
        if self.serialObj.runThread.is_alive():
            print(self.serialObj.runThread.is_alive())
            self.serialObj.exitProgram()
            print(self.serialObj.runThread.is_alive())
        # self.Shutdown()

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, new_status):
        if new_status != self.status:
            self._status = new_status
            self.timer = time.time()

    @property
    def DAG(self):
        return self._DAG

    @DAG.setter
    def DAG(self, new_DAG):
        self._DAG = new_DAG

    @property
    def RAG(self):
        return self._RAG

    @RAG.setter
    def RAG(self, new_RAG):
        self._RAG = new_RAG
        if self.status != 2:
            self._load = None
        elif self.RAG_avg30 >= self.upperTemp:
            if self._load != 2:
                self._load = 2
                msg = ["達到溫度上限，壓縮機全載。", self.status, self.DAG, self.RAG]
                ia.writeCsv(self.bound_log, msg)
        elif self.RAG_avg30 <= self.targetTemp:
            if self._load != 0:
                self._load = 0
                msg = ["達到目標溫度，壓縮機低載。", self.status, self.DAG, self.RAG]
                ia.writeCsv(self.bound_log, msg)
        elif ((self.RAG - self.controlTemp)*(self.controlTemp-self.old_RAG)) >= 0:
            if self._load != 1:
                self._load, self.qPrev = 1, 0
                msg = ["啟動熱負載預測控制。", self.status, self.DAG, self.RAG]
                ia.writeCsv(self.bound_log, msg)

    # def updateTemp(self):
    #     if not self.mongo.RAG == None:
    #         self.RAG = self.mongo.RAG
    #     if not self.mongo.DAG == None:
    #         self.DAG = self.mongo.DAG
    #     self.Tenv = 0
    #     self.old_RAG = self.mongo.RAG_avg30

    def updateSensor(self):
        l_RAG, l_DAG, l_left, l_right = [], [], [], []
        query = self.mycol.find().sort('_id', -1).limit(45)  # 180秒執行一次熱負載預測，db每4秒一筆，共取45筆
        for doc in query:
            l_RAG.append(doc.get("13"))
            l_DAG.append(doc.get("14"))
            l_left.append(doc.get("15"))
            l_right.append(doc.get("16"))

        self.RAG = round(l_RAG[0] * 0.1, 1)
        self.DAG = round(l_DAG[0] * 0.1, 1)

        # 回風溫度連續30秒高於-16啟動全載、低於-20啟動低載、過-18啟動熱負載預測
        self.RAG_avg30 = round(sum(l_RAG[0:7])/len(l_RAG[0:7]) * 0.1, 1)
        # 出風溫度連續10筆高於5度要關電熱
        self.DAG_min10 = round(min(l_DAG[0:10]) * 0.1, 1)
        if not max(l_left) == 0:
            self.leftDoor = round(1-(sum(l_left)/max(l_left)/len(l_left)), 4)
        if not max(l_right) == 0:
            self.rightDoor = round(
                1-(sum(l_right)/max(l_right)/len(l_right)), 4)
        # print("RAG:", self.RAG, "DAG:", self.DAG, "left:", self.leftDoor, "right:", self.rightDoor)
        self.avgDoor = round((self.leftDoor + self.rightDoor)/2, 5)
        self.Tenv = 0
        self.old_RAG = self.RAG_avg30

    def evalCapacity(self, Qpre, Qnow):  # 計算是否超過冷凍能力
        Qpre += Qnow
        if Qpre > self.capacity:
            Qpre -= self.capacity
            Qnow = self.capacity
        else:
            Qpre, Qnow = 0, Qpre

        return round(Qpre, 1), round(Qnow, 1)

    def heatloadPredictionControl(self):
        # qLoad = hl.accuHeatLoad(Fdoor, Tenv, interval)
        self.tempEnv = 26  # 實驗用，進行其他環境溫度實驗室要改
        if self.avgDoor < 0.0001:
            qLoad = hl.Q26(0.0001)
        else:
            qLoad = hl.Q26(self.avgDoor)
        # qLoad = hl.Q26(Fdoor)
        # qLoad = hl.Q29(Fdoor)

        self.qPrev, qLoad = self.evalCapacity(self.qPrev, qLoad)
        # calculate Hz
        hz = hl.QtoHz(qLoad)  # 60hz ~ 150hz
        # set Hz to BLDC
        print(f"Fdoor: {self.avgDoor}, qLoad: {qLoad}, hz: {hz}")
        self.mh.freq_new = int(hz)  # 調整壓縮機頻率
        msg = [self.tempEnv, self.avgDoor,
               self.RAG, qLoad, self.qPrev, hz]
        ia.writeCsv(self.heatload_log, msg)

    def boundControl(self):
        if self._load == 2:  # 全載
            self.mh.freq_new = 150
        elif self._load == 0:  # 低載
            self.mh.freq_new = 60
        elif self._load == 1:  # HPC
            return False

    def defrostProcess(self):
        if self.f_defrost == 0:  # stage 1
            self.f_defrost = 1
            self.mh.status, self.mh.f_status = 0, 1  # 關閉壓縮機
            rp.f_defrost = True
            rp.writeGPIO(rp.fan, 0)  # 關閉風扇
            self.f_heater = 0
            self.defrost_start = time.time()
            msg = ["啟動除霜。", self.status, self.DAG, self.RAG]
            ia.writeCsv(self.bound_log, msg)

        if self.f_defrost == 1:
            lasted = time.time() - self.defrost_start
            if lasted < self.defrost_t1:
                return ()
            if lasted < self.defrost_t2:  # stage 2
                if rp.writeGPIO(rp.valve, 0):
                    msg = ["關閉電磁閥。", self.status, self.DAG, self.RAG]
                    ia.writeCsv(self.bound_log, msg)
                if rp.writeGPIO(rp.EEV, 0):  # 關閉EEV
                    msg = ["關閉EEV。", self.status, self.DAG, self.RAG]
                    ia.writeCsv(self.bound_log, msg)
            elif lasted <= self.defrost_t3:
                if self.f_heater == 0 and self.f_heaterRe == 0:  # stage 3
                    rp.writeGPIO(rp.heater, 1)  # 開啟除霜電熱
                    msg = ["開啟除霜電熱。", self.status, self.DAG, self.RAG]
                    ia.writeCsv(self.bound_log, msg)
                    self.f_heater = 1
                if self.f_heater == 1:
                    if self.DAG_min10 >= self.defrost_heater:  # stage 4
                        rp.writeGPIO(rp.heater, 0)  # 關閉除霜電熱
                        self.f_heater, self.f_heaterRe = 0, 1
                        msg = ["關閉除霜電熱。", self.status, self.DAG, self.RAG]
                        ia.writeCsv(self.bound_log, msg)

            elif lasted <= self.defrost_t4:  # stage 5
                if rp.writeGPIO(rp.heater, 0):  # 關閉除霜電熱
                    msg = ["關閉除霜電熱。", self.status, self.DAG, self.RAG]
                    ia.writeCsv(self.bound_log, msg)
                    self.heater, self.f_heaterRe = 0, 1
                rp.f_defrost = False
                if rp.writeGPIO(rp.fan, 1):  # 開啟風扇
                    msg = ["開啟風扇。", self.status, self.DAG, self.RAG]
                    ia.writeCsv(self.bound_log, msg)
                    self.mh.freq_new = 60  # 設定壓縮機轉速頻率
                if rp.writeGPIO(rp.EEV, 1):  # 開啟EEV
                    msg = ["開啟EEV。", self.status, self.DAG, self.RAG]
                    ia.writeCsv(self.bound_log, msg)
                if rp.writeGPIO(rp.valve, 1):  # 開啟valve
                    msg = ["開啟valve。", self.status, self.DAG, self.RAG]
                    ia.writeCsv(self.bound_log, msg)

            else:  # stage 6
                self.mh.status, self.mh.f_status = 1, 1  # 開啟壓縮機
                msg = ["結束除霜，開啟壓縮機。", self.status, self.DAG, self.RAG]
                ia.writeCsv(self.bound_log, msg)
                self.f_defrost, self.f_heaterRe, self.defrost_recover = 0, 0, time.time()
                # self.f_compressor, self.compressor_start = 1, time.time()
                self.status, self._load = 2, 2  # 除霜完進運轉
                print("進運轉模式")

                # self.status = 1  # 除霜完進保護
                # print("進保護模式")

    def printConfig(self):
        print("熱負載頻率：", self.HPC_interval, " seconds")
        print("目標溫度：", self.targetTemp)
        print("啟動HPC門檻溫度：", self.controlTemp)
        print("啟動全載門檻溫度：", self.upperTemp)

    def compressorStart(self):
        lasted = time.time() - self.timer
        if lasted < 120:
            rp.writeGPIO(rp.fan, 1)  # 開風扇
            self.mh.freq_new = 60  # 設定頻率60hz
            rp.writeGPIO(rp.valve, 1)  # 開電磁閥
            rp.writeGPIO(rp.EEV, 1)  # 開EEV
        else:
            self.mh.status, self.mh.f_status = 1, 1  # 開壓縮機
            self.status, self._load = 2, 2
            print("進運轉機制")
            # self.status = 1
            # print("進保護機制")

    def protect(self):
        if (time.time() - self.timer) < 60:
            self.mh.freq_new = 60
            # 設定壓縮機60hz
        else:
            self.status, self._load = 2, 2  # 保護完進全載
            print("進運轉機制")

    def runHPCcontrol(self):
        sys_count = time.time()
        alg_count = sys_count
        # signal.signal(signal.SIGINT, self.signal_handler)
        self.mh.status, self.mh.f_status = 0, 1
        self.printConfig()
        self.defrost_recover = time.time() if self.defrost_recover == 0 else self.defrost_recover

        try:
            while self.mh.runThread.is_alive() and self.ic.runThread.is_alive() and not self.ic.f_shutdown:
                # 啟動機制[0]
                if self.status == 0:
                    self.compressorStart()
                # 保護機制[1]
                if self.status == 1:
                    self.protect()
                # 運轉機制[2]
                if self.status == 2:
                    # 變頻模式[0, 2] 低載、全載
                    if (time.time() - sys_count) >= self.sys_interval:
                        sys_count = time.time()
                        self.boundControl()
                    # 變頻模式[1] 熱負載預測控制演算法
                    if (time.time() - alg_count) >= self.HPC_interval:
                        if self._load == 1:
                            alg_count = time.time()
                            self.heatloadPredictionControl()
                # 除霜機制[3]
                if (time.time() - self.defrost_recover > self.defrost_int):
                    self.status = 3
                    self.defrostProcess()
                # update temperature and fdoor
                if (time.time() - self.temp_timer) > self.sys_interval:
                    self.updateSensor()
                    self.temp_timer = time.time()
                # 系統存活判斷
                if (time.time() - self.alive) >= 10:
                    # print(" TemRA: {0:.3f}".format(self.TempRA))
                    print(".", end="")
                    log = [ia.currentTime(self.defrost_recover), self.status, self.RAG,
                           self.DAG, self.avgDoor, self.mh.freq, self.mh.error]
                    ia.writelog(self.variables, log)
                    self.alive = time.time()

                if self.ic.status:
                    if self.ic.command == 1:
                        print(f'開啟開關門控制風扇功能')
                    elif self.ic.command == 0:
                        print(f'關閉開關門控制風扇功能')
                    rp.mode = self.ic.command
                    self.ic.status = False

                if self.ic.f_defrost:
                    self.defrost_recover -= self.defrost_int
                    self.ic.f_defrost = False

                time.sleep(0.5)

        finally:
            print("Program terminated.")
            self.Shutdown()
            sys.exit(0)

    def Shutdown(self):
        print("停止機制啟動")
        self.mh.status, self.mh.f_status = 0, 1  # 關閉壓縮機
        rp.writeGPIO(rp.fan, 0)  # 關風扇
        time.sleep(1)
        rp.writeGPIO(rp.heater, 0)  # 關電熱
        time.sleep(60)  # 等60s
        rp.writeGPIO(rp.EEV, 0)  # 關電子膨脹閥
        time.sleep(1)
        rp.writeGPIO(rp.valve, 0)  # 關電磁閥
        print("Terminated.")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--time', '-t', type=str,
                        required=False, help='defrost recovery time')
    opt = parser.parse_args()
    recover = None
    if not opt.time == None:
        recover = time.mktime(time.strptime(opt.time, "%Y %m %d %H:%M:%S"))

    # controller = con.sendSerial()
    # inp = com.inputCommand()
    inputCom = ic.inputCommand()
    # database = db.mongodb()
    modbus = modbusHandler.mbHandler()
    main = HPCcontrol(modbus, inputCom, recover)
    main.daemon = True
    main.start()

    while True:
        time.sleep(1)
    # ser = cp.hzSerial()
    # HPC = HPCcontrol()
