# -*- coding: utf-8 -*-
"""
Created on Wed Mar  1 16:38:39 2023

@author: A60384
"""

import numpy as np


def accuHeatLoad(freq, temp):
    freq = 0.0001 if freq == 0 else freq

    if temp <= 23:
        return Q23(freq)
    elif temp > 23 and temp <= 26:
        return interpolation(Q23(freq), Q26(freq), 23, 26, temp)
    elif temp > 26 and temp < 29:
        return interpolation(Q26(freq), Q29(freq), 26, 29, temp)
    elif temp >= 29:
        return Q29(freq)


def interpolation(h1, h2, t1, t2, t):

    return (h1 + (t-t1)*(h2-h1)/(t2-t1))


# 壓縮機曲線輸入Hz得到Q
def HztoQ(x):
    return -0.0865*x*x + 17.983*x + 2.9202

# 壓縮機曲線輸入Q得到Hz


def QtoHz(x):

    minimum = 10000

    for i in np.arange(30, 76, 1):
        # temp = -0.0633*i*i + 10.857*i + 293.65
        temp = -0.0865*i*i + 17.983*i + 2.9602

        if abs(x-temp) <= minimum:
            minimum, Hz = abs(x-temp), i

    return Hz*2


# 因x=0時為Q負無限大，可能要輸入一個極小數值(0.0001)取代0作為無開門之情境
# 23度熱負載曲線
def Q23(x):
    Q = 89.692*np.log(x) + 494.61 + 270
    if Q <= 270:
        return 270
    # elif Q >= 700:
    #     return 700
    else:
        return Q

# 26度熱負載曲線


def Q26(x):
    Q = 103.33*np.log(x) + 621.78 + 716
    if Q <= 716:
        return 716
    elif Q >= 961:
        return 961
    else:
        return Q

# 29度熱負載曲線


def Q29(x):
    Q = 83.034*np.log(x) + 548.22 + 424
    if Q <= 424:
        return 424
    # elif Q >= 700:
    #     return 700
    else:
        return Q


if __name__ == '__main__':
    print(HztoQ(150))
