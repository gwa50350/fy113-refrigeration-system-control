
import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))
import controller as con

if __name__ == '__main__':
    
    controller = con.sendSerial()
    
    print("輸入壓縮機功能測試指令：")
    print("0=停機", "1=開啟電磁閥", "60~150=壓縮機轉速", sep=os.linesep)
    
    while True:
        command = int(input())
        if command == 0 or command == 1 or (command >=60 and command <= 150):
            controller.cpValue = command
        else:
            print("請輸入正確測試指令")