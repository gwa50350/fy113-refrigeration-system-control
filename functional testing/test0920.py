#溫控器溫度設定
import serial ,time
import minimalmodbus


instrument = minimalmodbus.Instrument(port='/dev/ttyUSB0', slaveaddress=7)  
instrument.serial.baudrate = 9600
instrument.serial.bytesize = 8
instrument.serial.parity   = serial.PARITY_NONE
instrument.serial.stopbits = 1
instrument.serial.timeout = 1
instrument.mode = minimalmodbus.MODE_RTU 

def readTemp():
    value=instrument.read_register(0x0108)
    print(value)
