import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)
Lookback_Max = 180
Left_DOOR_Pin = 18
Right_DOOR_Pin = 16
Read_TimeDelay = 1
GPIO.setup(Left_DOOR_Pin, GPIO.IN)
GPIO.setup(Right_DOOR_Pin, GPIO.IN)

Left_DOOR_History = [0.0]*Lookback_Max
Right_DOOR_History = [0.0]*Lookback_Max

def Lookback_Data(Lookback_Num, functioncode=0):  # (回推次數, 是否開關門計算)
    try:
        nums_mean = sum(Lookback_Num)/float(Lookback_Max)
        # print(RAG_mean)
        if functioncode == 1:  # [開關門計算]
            nums_max = 0.0001 if max(
                Lookback_Num) == 0 else max(Lookback_Num)
            nums_mean = nums_mean/nums_max
            return 0 if nums_mean < 0.0001 else nums_mean
        else:  # [一般計算]
            return 0 if nums_mean < 0.0001 else nums_mean

    except Exception as e:
        print(e)
        
def calDoorFreq():
    Door_Left_mean = Lookback_Data(Left_DOOR_History, 1)
    Door_Right_mean = Lookback_Data(Right_DOOR_History, 1)
    return Door_Left_mean, Door_Right_mean

if __name__ == '__main__':

    while True:
        # [step1: 讀取GPIO]
        try:
            Left_DOOR_Pin_STATUS = GPIO.input(Left_DOOR_Pin)
            Right_DOOR_Pin_STATUS = GPIO.input(Right_DOOR_Pin)
            Left_DOOR_History.append(Left_DOOR_Pin_STATUS)  # 新值加入後
            Left_DOOR_History.pop(0)
            Right_DOOR_History.append(Right_DOOR_Pin_STATUS)
            Right_DOOR_History.pop(0)
            leftMean, rightMean = calDoorFreq()
            print("left door current: ", round(Left_DOOR_Pin_STATUS, 4), ", average: ", round(leftMean, 4))
            print("right door current: ", round(Right_DOOR_Pin_STATUS, 4), ", average: ", round(rightMean, 4))
        except Exception as e:
            print(e)
            print("reading door GPIO failed")

        time.sleep(Read_TimeDelay)
