import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))
import controller as con

if __name__ == '__main__':
    
    controller = con.sendSerial()
    
    print("輸入風扇功能測試指令：")
    print("0=壓縮機啟動flag", "1=壓縮機停止flag", "3=轉速模式1", "4=轉速模式2", "5=轉速模式3", "6=轉速模式4", sep=os.linesep)
    
    while True:
        command = int(input())
        if command == 0 or command == 1 or command == 3 or command == 4 or command == 5 or command == 6:
            controller.fanValue = command
        else:
            print("請輸入正確測試指令")