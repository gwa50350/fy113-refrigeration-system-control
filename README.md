# Refrigeration System Control

## Flowchart
![Flowchart](/images/flowchart.png "Flowchart"){width=450}


# 孫項七冷櫃實驗變頻風扇控制程式 fan.py / fanControl.ino

## 實驗說明
- 福佑變頻風扇由arduino控制轉速，依據冷櫃開關門以及壓縮機狀態改變轉速
- 共分為四種轉速實驗，如下圖

![風扇轉速實驗說明](/images/%E9%A2%A8%E6%89%87%E8%BD%89%E9%80%9F%E5%AF%A6%E9%A9%97%E8%AA%AA%E6%98%8E.png "風扇轉速實驗說明"){width=500}

- 轉速實驗1：不管開關門及壓縮機狀態，維持風扇轉速在最大值
- 轉速實驗2：關門時風扇運轉在最大值，開門時風扇轉速在最小值
- 轉速實驗3：關門時風扇運轉在最大值，開門時風扇停止

![風扇轉速實驗23](/images/%E9%A2%A8%E6%89%87%E8%BD%89%E9%80%9F%E5%AF%A6%E9%A9%9723.png "風扇轉速實驗23"){width=500}

- 轉速實驗4：關門時風扇運轉在最大值，開門時風扇停止，其次如冷櫃達目標溫度停機時（除霜狀態除外），風扇先運轉於最大值180秒，再停止直到壓縮機啟動。

![風扇轉速實驗4](/images/%E9%A2%A8%E6%89%87%E8%BD%89%E9%80%9F%E5%AF%A6%E9%A9%974.png "風扇轉速實驗4"){width=500}