int pin = 6;
int fan = 11;
int Relay_Pin = 12;
int counter = 0;
const byte AD01 = A1;
const byte AD02 = A2;
char receivedChar;
int Solenoid_valve01;
int Solenoid_valve02;
int fanspeed = 2;
int fanspeed_old = 2;
int door = 0;
int control = 0;
int mode = 3;        // 轉速實驗指令(對應轉速實驗1~4)
int comp_status = 0; // 壓縮機狀態指令(對應轉速4實驗，0->最高速；1->停止)
int doorCount = 0;   // 開關門保護，防止誤動作
// int val = 0;
const unsigned int MAX_INPUT = 10;

void setup()
{
  Serial.begin(9600);
  pinMode(pin, OUTPUT);
  pinMode(fan, OUTPUT);
  pinMode(Relay_Pin, OUTPUT);

  // compressor PWM setting
  OCR0A = 155; // initial compressor speed
  TCCR0A = 0;
  TCCR0B = 0;
  TCCR0A = (1 << COM0A0) | (1 << WGM01) | (1 << WGM00);
  TCCR0B = (1 << WGM02) | (1 << CS00) | (1 << CS02);

  // fan PWM setting
  TCCR2B = 0;
  TCCR2B |= (1 << CS21); //  TCCR1B |= (1 << CS11)
  analogWrite(11, 230);  // initial fan speed
}

void decoder(const char *data)
{
  Serial.print("[Arduino] Arduino receives command:");
  Serial.print(data);
  char *value = &data[1];

  if (data[0] == 'f')
  {
    Serial.println("--> send to fan.");
    fanHandler(value);
  }
  else if (data[0] == 'c')
  {
    Serial.println("--> send to compressor.");
    compHandler(value);
  }
}

void fanHandler(const char *data)
{
  int fancommand = atoi(data);
  if (fancommand > 1) // command 3, 4, 5, 6對應轉速實驗1, 2, 3, 4
  {
    mode = fancommand - 2;
    Serial.print("[Arduino] 改變風扇轉速實驗為");
    Serial.println(mode);
  }
  else // command 0, 1 對應轉速實驗4壓縮機啟停flag 0, 1
  {
    Serial.println("[Arduino] 壓縮機啟停flag");
    comp_status = fancommand;
  }
}

void compHandler(const char *data)
{
  int freq = atoi(data);

  if (freq == 0)
  {
    digitalWrite(Relay_Pin, LOW); // LOW
    OCR0A = 155;
    Serial.println("[Arduino] Compressor stoped.");
  }
  else if (freq == 1)
  { // turn on valve
    digitalWrite(Relay_Pin, HIGH);
    OCR0A = 155;
    Serial.println("[Arduino] Valve turned on.");
    delay(1000);
  }
  else if (freq == 60)
  {
    OCR0A = 128; // offset to exceed 60hz
    digitalWrite(Relay_Pin, HIGH);
    Serial.print("[Arduino] OCR0A: ");
    Serial.println(OCR0A);
  }
  else if (freq > 30)
  {
    digitalWrite(Relay_Pin, HIGH);
    int val = 8000000 / 1024 / freq - 1;
    OCR0A = val;
    Serial.print("[Arduino] OCR0A: ");
    Serial.println(val);
  }
  else
  {
    Serial.println("[Arduino] Invalid command.");
  }
}

void processIncomingByte(const byte inByte)
{
  static char input_line[MAX_INPUT];
  static unsigned int input_pos = 0;

  switch (inByte)
  {
  case '\n':                   // end of text
    input_line[input_pos] = 0; // terminating null byte
    decoder(input_line);
    input_pos = 0; // reset buffer for next time
    break;

  case '\r': // discard carriage return
    break;

  default:
    // keep adding if not full ... allow for terminating null byte
    if (input_pos < (MAX_INPUT - 1))
      input_line[input_pos++] = inByte;
    break;
  } // end of switch
} // end of processIncomingByte

void doorStatus(int val_fan)
{
  if (val_fan != door)
  {
    if (val_fan == 0)
    {
      Serial.println("[Arduino] 門關");
    }
    else if (val_fan == 1)
    {
      Serial.println("[Arduino] 門開");
    }
    door = val_fan;
  }
}

void fanControl(int comm) // 依據fanspeed改變風扇轉速
{
  if (comm != fanspeed_old)
  {
    switch (comm)
    {
    case 0: // 風扇停止
      Serial.println("[Arduino] 風扇關閉");
      analogWrite(11, 20.5);
      break;
    case 1: // 風扇10%
      Serial.println("[Arduino] 風扇運轉10%");
      analogWrite(11, 26);
      break;
    case 2: // 風扇90%
      Serial.println("[Arduino] 風扇運轉90%");
      analogWrite(11, 230);
      break;
    }
    fanspeed_old = comm;
  }
}

void fanAlgo() // 依據開關門、壓縮機啟停、轉速實驗改變fanspeed
{
  switch (mode)
  {
  case 1: // 實驗1：維持最高轉速
    fanControl(2);

    break;

  case 2: // 實驗2：開門時最低轉，其餘最高轉
    if (door == 1)
    {
      fanControl(1);
    }
    else
    {
      fanControl(2);
    }
    break;

  case 3: // 實驗3：開門時停止，其餘最高轉
    if (door == 1)
    {
      fanControl(0);
    }
    else
    {
      fanControl(2);
    }
    break;

  case 4: // 實驗4：開門時停止，其餘最高轉，停機指令時停止
    if (door == 0 && comp_status == 0)
    {
      fanControl(2);
    }
    else
    {
      fanControl(0);
    }
  }
}

void doorHandler()
{
  Solenoid_valve01 = analogRead(AD01); // 由A1腳讀入門磁閥訊號
  Solenoid_valve02 = analogRead(AD02); // 由A2腳讀入門磁閥訊號
  if (Solenoid_valve01 == 0 && Solenoid_valve02 == 0)
  {
    doorStatus(0);
    doorCount = 0;
  }
  else if (doorCount <= 2)
  {
    doorCount++;
  }
  else if (doorCount == 3)
  {
    doorStatus(1);
    doorCount = 0;
  }
}

void loop()
{
  // 讀取開關門狀態
  doorHandler();

  // 讀取系統控制指令
  while (Serial.available() > 0)
  {
    processIncomingByte(Serial.read());
  }

  // 依據開關門、壓縮機啟停、轉速實驗判斷輸出風扇轉速
  fanAlgo();

  delay(100);
}

// prescaler is set to 1024, therefore the lower bound of hz is 30
// hz = 50 -> OCR0A = 155 (停機)
// hz = 60 -> OCR0A = 128 (最低轉速)
