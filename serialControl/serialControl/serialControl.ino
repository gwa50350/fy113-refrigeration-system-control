int pin = 13;
int Relay_Pin = 12;
int counter = 0;
unsigned long delay_ON = 5000;
unsigned long delay_OFF = 10000;
int val = 0;
void setup() {
  Serial.begin(9600);
  pinMode(pin, OUTPUT);
  pinMode(Relay_Pin, OUTPUT);
}
void loop(){
  if(Serial.available()) {
    val = Serial.parseInt();
    Serial.print("H");Serial.println(val);
    if(val>2){
      delay_ON = (1000000/val)/2;
      delay_OFF = delay_ON;
    }
  }

  if(val==0){   // turn off compressor and valve
    digitalWrite(pin, LOW);
    digitalWrite(Relay_Pin, LOW);
  }
  else if(val==1){ // turn on valve
    digitalWrite(pin, LOW);
    digitalWrite(Relay_Pin, HIGH);
    delay(1000);
  }
  else{ //given hz, turn on compressor and valve
    digitalWrite(Relay_Pin, HIGH);
    if (delay_ON <1000) {
      digitalWrite(pin, HIGH);
      delayMicroseconds(delay_ON);
      digitalWrite(pin, LOW);
      delayMicroseconds(delay_OFF);
    }
    else{
      digitalWrite(pin, HIGH);
      delay(delay_ON/1000);
      digitalWrite(pin, LOW);
      delay(delay_OFF/1000);
    }
  }
}
//delayMicroseconds(100);
//60Hz -> 1800rpm
//66Hz -> 2000rpm
//94Hz -> 2800rpm
//100Hz -> 3000rpm
//120Hz -> 3600rpm
//150Hz -> 4500rpm