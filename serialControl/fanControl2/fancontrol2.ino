int pin = 11;
int Relay_Pin = 12;
int counter = 0;
const byte AD00 = A0;
const byte AD01 = A1;
const byte AD02 = A2;
char receivedChar;
int Solenoid_valve01;
int Solenoid_valve02;
int fanspeed = 2;
int fanspeed_old = 2;
int door = 0;
int command = 0;
// int control = 0;
int mode = 4;       // 轉速實驗指令(對應轉速實驗1~4)
int com_status = 0; // 壓縮機狀態指令(對應轉速4實驗，0->最高速；1->停止)
const unsigned int MAX_INPUT = 50;

void setup()
{
  Serial.begin(9600);
  pinMode(pin, OUTPUT);
  pinMode(Relay_Pin, OUTPUT);
  TCCR1B = 0;
  TCCR1B |= (1 << CS11);
  fanControl();
}

void sendPFM(const char *data)
{
  command = atoi(data);
  Serial.print("H");
  Serial.println(command);
  setCommand(command);
}

void processIncomingByte(const byte inByte)
{
  static char input_line[MAX_INPUT];
  static unsigned int input_pos = 0;

  switch (inByte)
  {
  case '\n':                   // end of text
    input_line[input_pos] = 0; // terminating null byte
    sendPFM(input_line);
    input_pos = 0; // reset buffer for next time
    break;

  case '\r': // discard carriage return
    break;

  default:
    // keep adding if not full ... allow for terminating null byte
    if (input_pos < (MAX_INPUT - 1))
      input_line[input_pos++] = inByte;
    break;
  } // end of switch
} // end of processIncomingByte

void doorStatus(int val)
{
  if (val != door)
  {
    if (val == 0)
    {
      Serial.println("門關");
    }
    else if (val == 1)
    {
      Serial.println("門開");
    }
    door = val;
  }
}

void setCommand(int val)
{
  if (val > 1) // command 3, 4, 5, 6對應轉速實驗1, 2, 3, 4
  {
    mode = val - 2;
  }
  else // command 0, 1 對應轉速實驗4壓縮機啟停flag 0, 1
  {
    com_status = val;
  }
}

void fanControl()
{ // 依據fanspeed改變風扇轉速
  switch (fanspeed)
  {
  case 0: // 風扇停止
    analogWrite(11, 20.5);
    break;
  case 1: // 風扇10%
    analogWrite(11, 26);
    break;
  case 2: // 風扇90%
    analogWrite(11, 230);
    break;
  }
}

void fanAlgo()
{ // 依據開關門、壓縮機啟停、轉速實驗改變fanspeed
  switch (mode)
  {
  case 1: // 實驗1：維持最高轉速
    fanspeed = 2;
    break;

  case 2: // 實驗2：開門時最低轉，其餘最高轉
    if (door == 1)
    {
      fanspeed = 1;
    }
    else
    {
      fanspeed = 2;
    }
    break;

  case 3: // 實驗3：開門時停止，其餘最高轉
    if (door == 1)
    {
      fanspeed = 0;
    }
    else
    {
      fanspeed = 2;
    }
    break;

  case 4: // 實驗4：開門時停止，其餘最高轉，停機指令時停止
    if (door == 0 && com_status == 0)
    {
      fanspeed = 2;
    }
    else
    {
      fanspeed = 0;
    }
  }
}

void loop()
{
  Solenoid_valve01 = analogRead(AD01); // 由A1腳讀入門磁閥訊號
  Solenoid_valve02 = analogRead(AD02); // 由A2腳讀入門磁閥訊號

  if (Solenoid_valve01 == 0 && Solenoid_valve02 == 0)
  {
    doorStatus(0);
  }
  else
  {
    doorStatus(1);
  }

  while (Serial.available() > 0)
  {
    processIncomingByte(Serial.read());
  }

  fanAlgo(); // 依據開關門、壓縮機啟停、轉速實驗改變fanspeed

  if (fanspeed != fanspeed_old)
  { // fanspeed有改變才會動作，減少運算時間
    if (fanspeed == 0)
    {
      Serial.println("風扇關閉");
    }
    else if (fanspeed == 1)
    {
      Serial.println("風扇運轉10%");
    }
    else if (fanspeed == 2)
    {
      Serial.println("風扇運轉90%");
    }
    fanspeed_old = fanspeed;
    fanControl();
  }

  delay(1000);
}

// prescaler is set to 1024, therefore the lower bound of hz is 30
// hz = 50 -> OCR0A = 155
// hz = 60 -> OCR0A = 12
