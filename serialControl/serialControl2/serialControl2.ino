int pin = 6;
int Relay_Pin = 12;
int counter = 0;
// int val = 0;
const unsigned int MAX_INPUT = 50;

void setup()
{
  Serial.begin(9600);
  pinMode(pin, OUTPUT);
  pinMode(Relay_Pin, OUTPUT);
  OCR0A = 155;
  TCCR0A = 0;
  TCCR0B = 0;
  TCCR0A = (1 << COM0A0) | (1 << WGM01) | (1 << WGM00);
  TCCR0B = (1 << WGM02) | (1 << CS00) | (1 << CS02);
}

void sendPFM(const char *data)
{
  int freq = atoi(data);
  Serial.print("H");
  Serial.println(freq);

  if (freq == 0)
  {
    digitalWrite(Relay_Pin, LOW);
    OCR0A = 155;
    Serial.println("Compressor stoped.");
  }
  else if (freq == 1)
  { // turn on valve
    digitalWrite(Relay_Pin, HIGH);
    OCR0A = 155;
    Serial.println("Valve turned on.");
    delay(1000);
  }
  else if (freq == 60)
  {
    OCR0A = 128; // offset to exceed 60hz
    digitalWrite(Relay_Pin, HIGH);
    Serial.print("OCR0A: ");
    Serial.println(OCR0A);
  }
  else if (freq > 30)
  {
    digitalWrite(Relay_Pin, HIGH);
    int val = 8000000 / 1024 / freq - 1;
    OCR0A = val;
    Serial.print("OCR0A: ");
    Serial.println(val);
  }
  else
  {
    Serial.println("Invalid command.");
  }
}

void processIncomingByte(const byte inByte)
{
  static char input_line[MAX_INPUT];
  static unsigned int input_pos = 0;

  switch (inByte)
  {
  case '\n':                   // end of text
    input_line[input_pos] = 0; // terminating null byte
    sendPFM(input_line);
    input_pos = 0; // reset buffer for next time
    break;

  case '\r': // discard carriage return
    break;

  default:
    // keep adding if not full ... allow for terminating null byte
    if (input_pos < (MAX_INPUT - 1))
      input_line[input_pos++] = inByte;
    break;
  } // end of switch
} // end of processIncomingByte

void loop()
{
  while (Serial.available() > 0)
  {
    processIncomingByte(Serial.read());
  }
}

// prescaler is set to 1024, therefore the lower bound of hz is 30
// hz = 50 -> OCR0A = 155
// hz = 60 -> OCR0A = 128