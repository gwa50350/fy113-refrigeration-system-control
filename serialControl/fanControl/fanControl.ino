int pin = 11;
int Relay_Pin = 12;
int counter = 0;
const byte AD00 = A0;
const byte AD01 = A1;
const byte AD02 = A2;
char receivedChar;
int Solenoid_valve01;
int Solenoid_valve02;
int fanspeed = 2;
int fanspeed_old = 2;
int door = 0;
int command = 0;
// int val ;
const unsigned int MAX_INPUT = 50;

void setup()
{
  Serial.begin(9600);
  pinMode(pin, OUTPUT);
  pinMode(Relay_Pin, OUTPUT);

  TCCR1B = 0;
  TCCR1B |= (1 << CS11);
}

// void sendPFM(const char* data) {
//   int freq = atoi(data);
//   Serial.print("H");
//   Serial.println(freq);

//   if (freq == 0) {
//     digitalWrite(Relay_Pin, LOW);
//     // OCR0A = 155;
//     Serial.println("Fan operates in 3000rpm.");
//   } else if (freq == 1) {  // turn on valve
//     digitalWrite(Relay_Pin, HIGH);
//     // OCR0A = 155;
//     Serial.println("Fan operates in 0rpm.");
//     delay(1000);
//   } else {
//     Serial.println("Invalid command.");
//   }
// }

void sendPFM(const char *data)
{
  command = atoi(data);
  Serial.print("H");
  Serial.println(command);
}

void processIncomingByte(const byte inByte)
{
  static char input_line[MAX_INPUT];
  static unsigned int input_pos = 0;

  switch (inByte)
  {
  case '\n':                   // end of text
    input_line[input_pos] = 0; // terminating null byte
    sendPFM(input_line);
    input_pos = 0; // reset buffer for next time
    break;

  case '\r': // discard carriage return
    break;

  default:
    // keep adding if not full ... allow for terminating null byte
    if (input_pos < (MAX_INPUT - 1))
      input_line[input_pos++] = inByte;
    break;
  } // end of switch
} // end of processIncomingByte

void doorStatus(int val)
{
  if (val != door)
  {
    if (val == 0)
    {
      Serial.println("門關");
    }
    else if (val == 1)
    {
      Serial.println("門開");
    }
    door = val;
  }
}
void fanControl()
{
  if (fanspeed == 0)
  {
    analogWrite(11, 20);
    // delayMicroseconds(10);
    // analogWrite(6, 10);
    // delayMicroseconds(490);
  }
  else if (fanspeed == 1)
  {
    analogWrite(11, 26);
    // delayMicroseconds(60);
    // analogWrite(6, 50);
    // delayMicroseconds(440);
  }
  else if (fanspeed == 2)
  {
    analogWrite(11, 230);
    // delayMicroseconds(450);
    // analogWrite(6, 240);
    // delayMicroseconds(50);
  }
}

void loop()
{
  Solenoid_valve01 = analogRead(AD01); // 由A1腳讀入門磁閥訊號
  Solenoid_valve02 = analogRead(AD02); // 由A2腳讀入門磁閥訊號

  if (Solenoid_valve01 == 0 && Solenoid_valve02 == 0)
  {
    doorStatus(0);
  }
  else
  {
    doorStatus(1);
  }

  while (Serial.available() > 0)
  {
    processIncomingByte(Serial.read());
  }

  if (door == 1)
  {
    fanspeed = 0;
  }
  else if (door == 0)
  {
    if (command == 0)
    {
      fanspeed = 2;
    }
    else if (command == 1)
    {
      fanspeed = 1;
    }
  }

  // while (Serial.available() > 0) {
  //   processIncomingByte(Serial.read());
  // }

  if (fanspeed != fanspeed_old)
  {
    if (fanspeed == 0)
    {
      Serial.println("風扇關閉");
    }
    else if (fanspeed == 1)
    {
      Serial.println("風扇運轉10%");
    }
    else if (fanspeed == 2)
    {
      Serial.println("風扇運轉90%");
    }
    fanspeed_old = fanspeed;
  }

  fanControl();
  delay(1000);
}

// prescaler is set to 1024, therefore the lower bound of hz is 30
// hz = 50 -> OCR0A = 155
// hz = 60 -> OCR0A = 12