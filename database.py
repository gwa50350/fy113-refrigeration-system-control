import pymongo
import sys
import time
import threading


class mongodb():
    def __init__(self):
        self.url = "mongodb://web:02750963@140.96.8.136:27017/"
        self.client = pymongo.MongoClient(self.url)

        self.mydb_Name = "itri"
        self.mydb = self.client[self.mydb_Name]
        self.mycol = self.mydb["MW100"]

        self.RAG = 0.0
        self.DAG = 0.0
        # self.RAG_min = 0.0
        # self.DAG_min = 0.0
        # self.RAG_max = 0.0
        # self.DAG_max = 0.0
        self.RAG_avg30 = 0.0
        self.DAG_avg10 = 0.0
        self.leftDoor = 0.0
        self.rightDoor = 0.0
        self.avgDoor = 0.0

        self.runThread = threading.Thread(target=self.updateSensor)
        self.runThread.daemon = True
        self.runThread.start()
        self.errorCount = 0

# print("目前存在的DB有  ", myclient.list_database_names())

    def initial(self):
        print("目前存在的DB有  ", self.client.list_database_names())
        # 印出所有DB的名稱
        print("目前存在的DB有  ", self.client.list_database_names())
        # 如果DB存在，則印出
        dblist = self.client.list_database_names()
        if self.mydb_Name in dblist:
            print("DB: {0}存在".format(self.mydb_Name))

    def queryRAG(self):
        last_document = self.mycol.find({'name': 'time'}, {'name': '13'}, {'name': '14'}, {'name': '15'}, {
            'name': '16'}).sort('_id', -1).limit(1)  # 讀取最後一筆資料
        # 將最後一筆資料提出並顯示
        for doc in last_document:
            print(doc)
        RAG = doc.get("13")
        print("RAG: ", RAG)
        return RAG

    def updateSensor(self):
        while True:
            RAG, DAG, left, right = [], [], [], []
            query = self.mycol.find().sort('_id', -1).limit(45)  # 180秒執行一次熱負載預測，db每4秒一筆，共取45筆
            for doc in query:
                RAG.append(doc.get("13"))
                DAG.append(doc.get("14"))
                left.append(doc.get("15"))
                right.append(doc.get("16"))

            self.RAG = round(RAG[0] * 0.1, 1)
            self.DAG = round(DAG[0] * 0.1, 1)

            # 回風溫度連續30秒高於-16啟動全載、低於-20啟動低載、過-18啟動熱負載預測
            self.RAG_avg30 = round(sum(RAG[0:7])/len(RAG[0:7]) * 0.1, 1)
            # 出風溫度連續10秒高於5度要關電熱
            self.DAG_avg10 = round(sum(DAG[0:3])/len(DAG[0:3]) * 0.1, 1)
            if not max(left) == 0:
                self.leftDoor = round(1-(sum(left)/max(left)/len(left)), 4)
            if not max(right) == 0:
                self.rightDoor = round(1-(sum(right)/max(right)/len(right)), 4)
            # print("RAG:", self.RAG, "DAG:", self.DAG, "left:", self.leftDoor, "right:", self.rightDoor)

            self.avgDoor = round((self.leftDoor + self.rightDoor)/2, 5)

            time.sleep(3)

    def exitProgram(self):

        sys.exit(0)


if __name__ == '__main__':

    db = mongodb()
    while True:
        time.sleep(1)
    # print(doc)
